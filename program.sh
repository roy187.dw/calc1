# Author ggordonutech
# We can add two numbers (5 and 3) using the sample execution below
# ./program.sh 5 3

# Store the 1st argument (eg. 5) in VAR1 and the second argument (eg. 3) in VAR2
VAR1=$1
VAR2=$2

# Check if the variable (environment) has been defined
if [ -z ${OUTPUT_FILE_NAME} ] # Returns True if variable OUTPUT_FILE_NAME is NOT defined
then
    # Define variable with default value
    OUTPUT_FILE_NAME="output.txt"
    echo "Using default output file name of '$OUTPUT_FILE_NAME'."
else
    echo "Using given file name of '$OUTPUT_FILE_NAME'"
fi



echo "We will attempt to add $VAR1 + $VAR2"

# Create (overwrites if it already exists) new file with output text
echo "We have done a lot of work" > $OUTPUT_FILE_NAME

# Calculate result
RESULT=$((VAR1+VAR2))

RESULT_OUTPUT="CALCULATION : $VAR1 + $VAR2 = $RESULT"

echo $RESULT_OUTPUT

# Append result output to file
echo $RESULT_OUTPUT  >> $OUTPUT_FILE_NAME
